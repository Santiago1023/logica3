package Forma1;

public class LDL {
    private NodoDoble primero;

    // constructor
    public LDL() {
        setPrimero(null);
    }
    // getters and setters
    public NodoDoble getPrimero() {
        return primero;
    }

    public void setPrimero(NodoDoble primero) {
        this.primero = primero;
    }

    public NodoDoble primerNodo() {
        return primero;
    }


}
