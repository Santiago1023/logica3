package Forma1;

import Tripletas.tripleta;
public class matrizForma1 {

    private NodoDoble mat;

    // constructor
    public matrizForma1(int  m,  int  n)   {
        tripleta t = new   tripleta(m, n, null);
        mat = new NodoDoble(t);
        t.setValor(mat);
        mat.setDato(t);
    }
    public NodoDoble nodoCabeza(){
        return mat;
    }


    public NodoDoble primerNodo(){
        tripleta   tp = (tripleta)mat.getDato();
        NodoDoble p = (NodoDoble)tp.getValor();
        return p;
    }


    public boolean   esVacia(){
        NodoDoble p = primerNodo();
        return  p == mat;
    }

    public boolean   finDeRecorrido(NodoDoble p){
        return   p == mat;
    }


    public void   muestraMatriz() {
        int qf, qc, qv;
        NodoDoble p, q;
        tripleta tq, tp;
        p = primerNodo();
        while (! finDeRecorrido(p)){
            q = p.getLd();
            while (q != p) {
                tq = (tripleta) q.getDato();
                qf = tq.getFila();
                qc = tq.getColumna();
                qv = (int) tq.getValor();
                System.out.println(qf +""+ qc +""+ qv);
                q = q.getLd();
            }
            tp = (tripleta) p.getDato();
            p = (NodoDoble) tp.getValor();


        }
    }

    public void   construyeNodosCabeza(){
        int mayor, i, m,n;
        NodoDoble x, ultimo;
        tripleta   t;
        ultimo = nodoCabeza();
        t = (tripleta)ultimo.getDato();
        m = t.getFila();
        n = t.getColumna();
        mayor = m;
        if    (n > m) {
            mayor = n;
        }
        for   (i = 1;  i <= mayor;  i++) {
            t = new   tripleta(i, i, nodoCabeza());
            x = new NodoDoble(t);
            x.setLd(x);
            x.setLi(x);
            t = (tripleta)ultimo.getDato();
            t.setValor(x);
            ultimo.setDato(t);
            ultimo = x;
        }
    }

    public void   conectaPorFilas(NodoDoble x){
        NodoDoble p, q, anterior;
        tripleta tp, tq, tx;
        int i;
        tx = (tripleta)x.getDato();
        p = primerNodo();
        for (i = 1; i < tx.getFila(); i++) {
            tp = (tripleta)p.getDato();
            p = (NodoDoble)tp.getValor();
        }
        anterior = p;
        q = p.getLd();
        tq = (tripleta)q.getDato();
        while ((q != p)  &&  (tq.getColumna() <  tx.getColumna())){
            anterior = q;
            q = q.getLd();
            tq = (tripleta)q.getDato();
        }
        anterior.setLd(x);
        x.setLd(q);
    }

    public void   conectaPorColumnas(NodoDoble x){
        NodoDoble p, q, anterior;
        tripleta    tp, tq, tx;
        int i;
        tx = (tripleta)x.getDato();
        p = primerNodo();
        for  (i = 1; i < tx.getColumna(); i++) {
            tp = (tripleta) p.getDato();
            p = (NodoDoble) tp.getValor();
        }
        anterior = p;
        q = p.getLi();
        tq = (tripleta)q.getDato();
        while ((q != p)  &&  (tq.getFila() <  tx.getFila())){
            anterior = q;
            q = q.getLi();
            tq = (tripleta)q.getDato();
        }
        anterior.setLi(x);
        x.setLi(q);
    }

    /*
read(m, n)
matrizForma1  a =  new   matrizForma1(m, n)
a.construyeNodosCabeza()
mientras haya datos por leer  haga
    read(f, c, v)
	t = new  tripleta(f, c, v)
	x = new  nodoDoble(t)
	a.conectaPorFilas(x)
	a.conectaPorColumnas(x)
Fin(mientras)

    *
    * */


}
