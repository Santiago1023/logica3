package Forma1;

public class NodoDoble {

    private Object dato;
    private NodoDoble Li, Ld;

    public NodoDoble(Object dato) {
        setDato(dato);
        Li = null;
        Ld = null;

    }

    public void asginaLi(NodoDoble y) {
        setLi(y);
    }

    public void asginaLd(NodoDoble y) {
        setLd(y);
    }

    public Object getDato() {
        return dato;
    }

    public void setDato(Object d) {
        this.dato = d;
    }

    public NodoDoble getLi() {
        return Li;
    }

    public void setLi(NodoDoble li) {
        Li = li;
    }

    public NodoDoble getLd() {
        return Ld;
    }

    public void setLd(NodoDoble ld) {
        Ld = ld;
    }
}
