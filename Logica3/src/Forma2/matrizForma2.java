package Forma2;

import Forma1.NodoDoble;
import Tripletas.tripleta;

public class matrizForma2 {

    private NodoDoble mat;

    // constructor
    public matrizForma2(int  m,  int  n) {
        tripleta t = new   tripleta(m, n, null);
        mat = new   NodoDoble(t);
        //tripleta tx = new  tripleta(null, null, null);
        tripleta tx = new  tripleta(0, 0, null);
        NodoDoble x = new  NodoDoble(tx);
        x.setLi(x);
        x.setLd(x);
        mat.setLd(x);

    }
    public NodoDoble   primerNodo(){
        return   mat;
    }

    public NodoDoble   nodoCabeza(){
        return   mat.getLd();
    }

    public boolean   esVacia(){
        NodoDoble   p = mat.getLd();
        return  (p.getLi() == p   &&   p.getLd() == p);
    }

    public boolean   finDeRecorrido(NodoDoble  p){
        return   p == nodoCabeza();
    }

    public void   muestraMatriz(){
        int qf, qc, qv;
        NodoDoble   q;
        tripleta   tq;
        q = nodoCabeza().getLd();
        while   (! finDeRecorrido(q)) {
            tq = (tripleta)q.getDato();
            qf = tq.getFila();
            qc = tq.getColumna();
            qv = (int) tq.getValor();
            System.out.println(qf + "" + qc + "" + qv);
            q = q.getLd();
        }
    }

    public void   conectaPorFilasForma2(NodoDoble   x){
        NodoDoble    p, q, anterior;
        tripleta    tq, tx;
        int i;
        tx = (tripleta)x.getDato();
        p = nodoCabeza();
        anterior = p;
        q = p.getLd();
        tq = (tripleta)q.getDato();
        while  (q != p  &&  tq.getFila() <  tx.getFila()){
            anterior = q;
            q = q.getLd();
            tq = (tripleta)q.getDato();

        }
        while  (q != p  &&  tq.getFila() ==  tx.getFila()  && tq.getColumna() <  tx.getColumna() ) {
            anterior = q;
            q = q.getLd();
            tq = (tripleta)q.getDato();
        }
        anterior.setLd(x);
        x.setLd(q);

    }

    public void   conectaPorColumnasForma2(NodoDoble   x){
        NodoDoble    p, q, anterior;
        tripleta    tq, tx;
        int i;
        tx = (tripleta)x.getDato();
        p = nodoCabeza();
        anterior = p;
        q = p.getLi();
        tq = (tripleta)q.getDato();
        while  (q != p  &&  tq.getColumna() <  tx.getColumna()) {
            anterior = q;
            q = q.getLi();
            tq = (tripleta)q.getDato();

        }
        while  (q != p  &&  tq.getColumna() ==  tx.getColumna()  && tq.getFila() <  tx.getFila()){
            anterior = q;
            q = q.getLi();
            tq = (tripleta)q.getDato();
        }
        anterior.setLi(x);
        x.setLi(q);

    }


    /*
    read(m, n)
2.	matrizForma2  a =  new   matrizForma2(m, n)
3.	mientras haya datos por leer  haga
4.		read(f, c, v)
5.		t = new  tripleta(f, c, v)
6.		x = new  nodoDoble(t)
7.		a.conectaPorFilasForma2(x)
8.		a.conectaPorColumnasForma2(x)
9.	Fin(mientras)

     */






}
