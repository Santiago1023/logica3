package Tripletas;

public class matrizEnTripletas {

    private tripleta V[];

    /*constructor
            recibe una Tripletas.tripleta con las dimensiones de la matriz
            y en el campo valor tiene un cero, indicando que la matriz esta vacia
            V es el vector de tamaño n*m + 2
            en la posicion V[0] almacenamos tamaños de la matriz y elementos diferentes de 0
     */
    //todo
    public matrizEnTripletas(tripleta t){

        int m = t.getFila();
        int n = t.getColumna();
        int p = m * n + 2 ;
        int  i;
        V = new tripleta[p];
        V[0] = t;
        for  (i = 1; i < p; i++) {
            V[i] = null;
        }
    }

    // este metodo simplemente asigna la Tripletas.tripleta enviada como parametro a la posicion i del vector V
    public void   asignaTripleta(tripleta tx, int  i){
        V[i] = tx;
    }

    // actualiza el campo de valor de la Tripletas.tripleta que esta en V[0]
    void  asignaNumeroTripletas(int  n){
        tripleta  t = V[0];
        t.setValor(n);
        V[0] = t;
    }

    //Retorna el número de filas de la matriz que se está representando
    public int  retornaNumeroFilas(){
        tripleta t = V[0];
        return t.getFila();
    }

    //Retorna el número de columnas de la matriz que se está representando
    public int retornaNumeroColumnas(){
        tripleta t = V[0];
        return t.getColumna();
    }

    //Retorna el número de elementos diferentes de cero de la matriz que se está representando
    public int retornaNumeroTripletas(){
        tripleta  t = V[0];
        return (int) t.getValor();
    }

    //Retorna la Tripletas.tripleta que se halla en la posición i del vector V
    public tripleta retornaTripleta(int  i){
        return  V[i];
    }

    //Simplemente se recorre el vector de tripletas escribiendo los datos contenidos en cada Tripletas.tripleta
    public void  muestraMatrizEnTripletas(){
        int i =1;
        tripleta t = retornaTripleta(0);
        int datos = (int) t.getValor();
        while  (i <= datos){
            System.out.println("Filas " + V[i].getFila() + ", Columnas " + V[i].getColumna() + ", Valor " + V[i].getValor() );
            i++;
        }
    }

    public void  insertaTripleta(tripleta ti){
        int i,j,p;
        tripleta t, tx;
        tx = retornaTripleta(0);
        p = (int) tx.getValor();
        i=1;
        t = retornaTripleta(i);

        while (i <= p && t.getFila() < ti.getFila()) {
            i++;
            t = retornaTripleta(i);
        }
        while(i <= p && t.getFila() == ti.getFila()  && t.getColumna()  <  ti.getColumna()){
            i++;
            t = retornaTripleta(i);
        }
        // todo hacer lo correspondiente
        if(ti.getColumna() == V[i].getColumna() && ti.getFila() == V[i].getFila()){
            System.out.println("quiere reemplazarla o no?");
        }
        p = p + 1;
        j= p-1;

        while  (j >= i){
            V[j + 1] = V[j];
            j = j-1;
        }
        V[i] = ti;

        asignaNumeroTripletas(p);
    }

    public matrizEnTripletas   suma(Tripletas.matrizEnTripletas  b){
        int ma, na, mb, nb, p, q, i, j, k, ss, fi, fj, ci, cj, vi, vj;
        tripleta  ti, tj, tx;
        ma = retornaNumeroFilas();
        na = retornaNumeroColumnas();
        mb = b.retornaNumeroFilas();
        nb = b.retornaNumeroColumnas();
        p = retornaNumeroTripletas();
        q = b.retornaNumeroTripletas();
        if  ((ma != mb) || (na != nb)){
            System.out.println("Matrices de diferentes dimensiones no se puede sumar");
            return  null;
        }
        ti = new  tripleta(ma, na, 0);
        matrizEnTripletas  c = new Tripletas.matrizEnTripletas(ti);
        i=1;
        j=1;
        k=0;
        while  ((i <= p)  &&  (j <= q)){
            ti = retornaTripleta(i);
            tj = b.retornaTripleta(j);
            fi = ti.getFila();
            fj = tj.getFila();
            k = k + 1;
            switch (comparar(fi,fj)){
                case -1: // fila de A menor que fila de b
                    c.asignaTripleta(ti, k);
                    i = i + 1;
                    break;
                case +1: // fila de A mayor que fila de b
                    c.asignaTripleta(tj, k);
                    j = j + 1;
                    break;
                case 0: // fila de A igual a fila de b
                    ci = ti.getColumna();
                    cj = tj.getColumna();
                    switch (comparar(ci,cj)){
                        case -1: // columna de A menor que columna de b
                            c.asignaTripleta(ti, k);
                            i = i + 1;
                            break;
                        case +1 : // columna de A mayor que columna de b
                            c.asignaTripleta(tj, k);
                            j = j + 1;
                            break ;
                        case 0:  // columna de A igual a columna de b
                            vi = (int) ti.getValor();
                            vj = (int) tj.getValor();
                            ss = vi + vj;
                            if   (ss  !=  0) {
                                tx = new tripleta(fi, ci, ss);
                                c.asignaTripleta(tx, k);
                            }

                            else {
                                k =k-1;
                            }

                            i = i + 1;
                            j = j + 1;
                            break;
                    }
            }
        }
        while  (i <= p){
            ti = retornaTripleta(i);
            k = k + 1;
            c.asignaTripleta(ti, k);
            i = i + 1;
        }
        while  (j <= q){
            tj = b.retornaTripleta(j);
            k = k + 1;
            c.asignaTripleta(tj, k);
            j = j + 1;
        }
        c.asignaNumeroTripletas(k);
        return c ;
    }

    int comparar(int d1, int d2){
        if  (d1 < d2) {
            return -1;
        }
        if   (d1 == d2) {
            return  0;
        }
        return +1;

    }

    public matrizEnTripletas   traspuesta(){
        int i, p, f, c, v;
        tripleta ti;
        p = retornaNumeroTripletas();
        ti  =  new tripleta(retornaNumeroColumnas(), retornaNumeroFilas(), 0);
        matrizEnTripletas  b = new   matrizEnTripletas(ti);
        i = 1;
        while   (i <= p){
            ti = retornaTripleta(i);
            f = ti.getColumna();
            c = ti.getFila();
            v = (int) ti.getValor();
            ti = new tripleta(f, c, v);
            b.insertaTripleta(ti);
            i = i + 1;
        }
        return b;
    }

    public matrizEnTripletas   traspuestaMedia(){
        int i, j, k, m, n, p, f, c, v;
        tripleta tj, tx;
        m = retornaNumeroFilas();
        n =  retornaNumeroColumnas();
        p =  retornaNumeroTripletas();
        tx = new tripleta(n, m, p);
        matrizEnTripletas  b = new   matrizEnTripletas(tx);
        k = 0;
        for  (i = 1; i <= n; i++) {
            for (j = 1; j <= p; j++) {
                tj = retornaTripleta(j);
                f = tj.getFila();
                c = tj.getColumna();
                v = (int) tj.getValor();
                if (c == i) {
                    k = k + 1;
                    tx = new tripleta(c, f, v);
                    b.asignaTripleta(tx, k);
                }
            }
        }
        return  b;
    }

    public matrizEnTripletas  traspuestaRapida(){
        int m, n, p, i, j, s[], t[];
        tripleta ti, tx;
        m = retornaNumeroFilas();
        n =  retornaNumeroColumnas();
        p =  retornaNumeroTripletas();
        ti = new tripleta(n, m, p);
        matrizEnTripletas  b = new  matrizEnTripletas(ti);
        s = new   int[n + 1];
        t = new   int[n + 1];
        for(i = 1; i <= n; i++) {
            s[i] = 0;
        }
        for(i = 1; i <= p; i++) {
            ti = retornaTripleta(i);
            s[ti.getColumna()] = s[ti.getColumna()] + 1;

        }
        t[1] = 1;
        for(i = 2; i <= n; i++) {
            t[i] = t[i-1] + s[i-1];
        }
        for(i = 1; i <= p; i++) {
            ti = retornaTripleta(i);
            j = ti.getColumna();
            tx = new tripleta(j, ti.getFila(), ti.getValor());
            b.asignaTripleta(tx, t[j]);
            t[j] = t[j] + 1;
        }
        return  b;

    }







}
