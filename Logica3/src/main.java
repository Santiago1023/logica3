import Forma1.NodoDoble;
import Forma1.matrizForma1;
import Forma2.matrizForma2;
import Tripletas.matrizEnTripletas;
import Tripletas.tripleta;


public class main {


    public static void main(String[] args) {
        int ma=0;
        int mb=0;
        int na=0;
        int nb=0;


        // creo las dimensiones de las matrices random , y verifico que tengan dimensiones adecuadas para poder multiplicar
        boolean sw = false;
        while(!sw){
            ma = (int) (Math.random()*8 + 1);
            na = (int) (Math.random()*8 + 1);
            mb = (int) (Math.random()*8 + 1);
            nb = (int) (Math.random()*8 + 1);
            if (na==mb){
                sw = true;
            }
        }

        //creo las matrices
        int[][] matrizA = new int[ma][na];
        int[][] matrizB = new int[mb][nb];
        int[][] matrizC = new int[ma][nb];

        // voy a llenar la matriz de 0, como maximo la mitad de los elementos
        int elementosA = (int) (Math.random()*(ma*na) / 2 );
        int elementosB = (int) (Math.random()*(mb*nb) / 2 );

        //distribuyo los datos para la matrizA
        for(int elementosi=1; elementosi<=elementosA;elementosi++){
            int i = (int) (Math.random()*ma);
            int j = (int) (Math.random()*na);
            int dato = (int) (Math.random()*10);
            if(matrizA[i][j]==0){
                matrizA[i][j]= dato;
            }
            else{
                elementosA -=1;
            }
        }

        //distribuyo los datos para la matrizB
        for(int elementosi=1; elementosi<=elementosB;elementosi++){
            int i = (int) (Math.random()*mb);
            int j = (int) (Math.random()*nb);
            int dato = (int) (Math.random()*10);
            if(matrizB[i][j]==0){
                matrizB[i][j]= dato;
            }
            else{
                elementosB -=1;
            }
        }

        System.out.println("esta es la matrizA, con dimensiones(" + ma + "," + na + ")");
        for(int iii=0; iii<ma; iii++){
            String str = "";
            for(int jjj=0; jjj<na; jjj++){
                str = str + matrizA[iii][jjj];
            }
            System.out.println(str);
        }

        System.out.println("esta es la matrizB, con dimensiones(" + mb + "," + nb + ")");
        for(int iii=0; iii<mb; iii++){
            String str = "";
            for(int jjj=0; jjj<nb; jjj++){
                str = str + matrizB[iii][jjj];
            }
            System.out.println(str);
        }


        // creo la representacion de la matrizA en forma1
        matrizForma1 a = new matrizForma1(ma,na);
        a.construyeNodosCabeza();
        for(int i=0;i<ma;i++){
            for(int j=0; j<na;j++){
                if(matrizA[i][j]!=0){
                    tripleta t = new tripleta(ma,na,matrizA[i][j]);
                    NodoDoble x = new NodoDoble(t);
                    a.conectaPorFilas(x);
                    a.conectaPorColumnas(x);

                }
            }
        }

        // creo la representacion de la matrizB en forma2
        matrizForma2 b = new matrizForma2(mb,nb);
        for(int i=0;i<mb;i++){
            for(int j=0; j<nb;j++){
                if(matrizB[i][j]!=0){
                    tripleta t = new tripleta(mb,nb,matrizB[i][j]);
                    NodoDoble x = new NodoDoble(t);
                    b.conectaPorFilasForma2(x);
                    b.conectaPorColumnasForma2(x);

                }
            }
        }

        //creamos la matriz en tripletas, posiblemente habrán espacios en blanco
        tripleta t = new tripleta(ma,nb,0);
        matrizEnTripletas c = new matrizEnTripletas(t);
        NodoDoble cab = a.nodoCabeza();
        NodoDoble p = a.primerNodo();
        NodoDoble q = b.nodoCabeza();
        //NodoDoble qq = q.getLi();
        NodoDoble pp = p.getLd();
        NodoDoble qq;
        tripleta y ;
        tripleta tp;
        tripleta tq;
        int filaA = 0;
        int filaB = 0;
        int columnaA = 0;
        int columnaB = 0;
        int datoA = 0;
        int datoB = 0;


        //aqui hacemos la multiplicacion
        int i = 0;
        int j = 0;
        int mult = 0;


        //if(i<ma){
        while(i<ma){
            //while(pp!=p){
            while(pp!=p){
                //pp = p.getLd();
                tp = (tripleta) pp.getDato();
                filaA = tp.getFila();
                columnaA = tp.getColumna();
                datoA = (int) tp.getValor();


                //if(j<nb){
                while(j<nb){
                    qq = q.getLi();
                    mult = 0;
                    while(qq!=q){
                        tq = (tripleta) qq.getDato();
                        filaB = tq.getFila();
                        columnaB = tq.getColumna();
                        datoB = (int) tq.getValor();
                        if(columnaA==filaB && columnaB==j){
                            mult = mult + (datoA*datoB);
                            matrizC[i][j] = matrizC[i][j]+ mult;
                            break;
                        }
                        qq = qq.getLi();
                    }
                    //matrizC[i][j] += mult;
                    j++;
                }

                pp = pp.getLd();
                j = 0;
            }
            y = (tripleta) p.getDato();
            p = (NodoDoble) y.getValor();
            i++;
        }

        // aqui con la matriz c, llenamos el vector de tripletas
        int k =1;
        for(int ii =0; i<ma; i++){
            for(int jj =0 ; j<nb ; j++){
                if(matrizC[ii][jj]!=0){
                    tripleta tt = new tripleta(ii, jj, matrizC[ii][jj]);
                    c.asignaTripleta(tt,k);
                    k++;
                }
            }
        }


        System.out.println("esta es la matrizA, con dimensiones(" + ma + "," + na + ")");
        for(int iii=0; iii<ma; iii++){
            String str = "";
            for(int jjj=0; jjj<na; jjj++){
                 str = str + matrizA[iii][jjj];
            }
            System.out.println(str);
        }

        System.out.println("esta es la matrizB, con dimensiones(" + mb + "," + nb + ")");
        for(int iii=0; iii<mb; iii++){
            String str = "";
            for(int jjj=0; jjj<nb; jjj++){
                str = str + matrizB[iii][jjj];
            }
            System.out.println(str);
        }

        System.out.println("esta es la matrizC, con dimensiones(" + ma + "," + nb + ")");
        for(int iii=0; iii<ma; iii++){
            String str = "";
            for(int jjj=0; jjj<nb; jjj++){
                str = str + matrizC[iii][jjj];
            }
            System.out.println(str);
        }










    }




}
